# Tp4

## Materia
Arquitectura de Computadoras año 2022
## Autores
Quiroga Gonzalo, Villarruel Enzo

## Profesores
Airabella Migue Andres, Astri Eith Andrada

## Description
El proyecto consiste en armar un procesador Risc-v, mediante la implementación de codigos previsto por la catedra.

## Ejercicio 1

Se necesita armar un datapatch para cada instrucción que se muestra en la Figura Nº 1.

![Figura Nº 1](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Set_instrucciones.png) 


En la Figura Nº 2, se puede observar las instrucciones de Tipo R. Como son la suma, resta, multiplicación entre otros.


![FIGURA Nº 2 Tipo R](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Tipo_R.jpg)

En La Figura Nº 3, tenemos las instrucciones de tiempo inmediato, como lo son addi, lw.

![Figura Nº 3 Tipo I](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Tipo_I.jpg )


En la Figura Nº 4, tenemos las instrucciones de tipo S, como son sw, sh, etc.

![Figura Nº 4 Tipo S](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Tipo_S.jpg)

Para las instrucciones de tipo SB, se muestra en el datapath de la Figura Nº5.

![Figura Nº 5. Tipo SB](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Tipo_SB.jpg)


## Ejercicio 2

Con todos los datapaths realizados en el ejercicio naterior, se pueden unir para así obtener un unico datapath. 

En la Figura Nº 6, podemos observarlo con todas las intrucciones en conjunto. 

![Figura Nº 6. Datapatch Completo](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Completo.jpeg)

La unidad de control se puede ver en la Figura Nº 7.

![Figura Nº 7. Unidad de control](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Unidad_de_control.jpeg)

## Ejercicio 3

Se realizo una tabla de verdad de la unidad de control para cada tipo de instrucción como se muestra en la Figura Nº 8.


![Figura Nº 8 Tabla de control](https://gitlab.com/quiroga-gonza/tp4/-/blob/main/Img_Datapath/Tabla_de_verdad.jpeg)

## Ejercicio 4 
Creamos el repositorio para subir los archivos y los datapaths. 



