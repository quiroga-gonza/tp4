# Tp5


## Materia
Arquitectura de Computadoras año 2022
## Autores
Quiroga Gonzalo, Villarruel Enzo

## Profesores
Airabella Migue Andres, Astri Eith Andrada

## Description 
El proyecto consiste en la implementacion de un microprocesador basado en la arquitectura RISC-V, esta se caracteriza por ser un conjunto de instrucciones libre y gratuito. Las aplicaciones en las que se utiliza a RISC-V tienen como regla ser rapidas, pequeñas y de bajo consumo. Por lo que para cumplir con estas exigencias todas las instrucciones tienden a ser lo mas simples posibles.
A la hora de realizar esta implementacion se debe tener en cuenta que el desarrollo del proyecto requiere el uso del lenguaje de programacion VHDL para el diseño de los distintos componentes del RISC-V, mientras que por otro lado para el testeo de cada uno de estos bloques se hace uso de python.


## Ejercicio 1

Para la resolucion del ejercicio uno se siguio paso a paso la guia brindada por la catedra donde se realizo la clonacion del repositorio donde se encuentran los distintos componentes del RISC-V.
La razon por la que se  realiza esta tarea es para evitar la modificacion accidental del codigo original del proyecto.
Estos componentes allarse en el siguiente link:

https://gitlab.com/unsl-up/risc-v 

Una vez el repositorio se encuentre en el ordenador y enlazado a una cuenta en github se procede a la modificacion del mismo.

## Ejercicio 2
Los IP cores se encuentran en la carpeta SCR, mientras que el directorio de Test está en test_phyton.

Una vez descargado el repositorio se puede encontrar en el mismo varias carpetas, estas son:

* Docker: contiene los archivos ejecutables para el incio del programa docker (Crea contenedores portatiles para aplicaciones) en distintas distribuciones, en este caso en particular se utilizo a "dockeshell.mac" ya que se realizo el proyecto en "".
* src: Esta carpeta contiene el codigo correspondiente a los distintos componentes que componen al RISC-V.
* test_python: Esta carpeta contiene todos los archivos necesarios para el testeo de cada componente del RISC-V.
* doc: Con manual para la instalación. 

##Ejercicio 3

Una vez que la terminal esta posicionada en la consola de docker y luego en el directorio del banco de registros, se utiliza el comando "make" para ejecutar el testeo del bloque en cuestion.
El testeo devolvio el mensaje "pass" indicando un funcionamiento correcto.

## Ejercicio 4
Observando el codigo correspondiente al componente ALU, se determino el faltante de distintas funciones basicas para el correcto funcionamiento de la ALU. Estas funciones son

.BEQ Comparacion para determinar igualdad
.BNE Comparacion para determinar igualdad
.BGE Dados "A" y "B" determina si "A">B
.BLTU Dados los unsigned "A" y "B" determina si "A"<"B"
.BGUE Dados los unsigned "A" y "B" determina si "A">="B"

Una vez colocadas estas funciones en el codigo "ALU.vhd", es necesario realizar cambios dentro del test bench para realizar el testeo de las nuevas funciones.
Todos estos cambios se ven dentro de los codigos "ALU.vhd" y "ALU_tb.py".

## Ejercicio 5
Se procedio a realizar un estructura de directorios, para poder copiar los IP Cores con sus respectivos test, para luego poder integrar todos los IP-Cores y podes formar el microprocesador deseado.
